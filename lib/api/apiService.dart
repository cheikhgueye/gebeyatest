


import 'dart:convert';
import 'dart:io';
import 'package:gebeya_test/const.dart';

class ApiService{
  HttpClient client = new HttpClient();
  getMakes() async{
    try {
      client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
      HttpClientRequest request = await client.getUrl(Uri.parse('${APIConstants.API_BASE_URL}inventory/make?popular=true'));
      request.headers.set('content-type', 'application/json');
      HttpClientResponse response = await request.close();
      String reply = await response.transform(utf8.decoder).join();
      final Map<String, String> headers = {
        "content-type":"application/json",
      };
      if(response.statusCode==200){
        return  jsonDecode(reply)["makeList"];
      } else {

      }

    } finally {

      client.close();
    }

  }
}