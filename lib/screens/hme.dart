
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gebeya_test/api/apiService.dart';
import 'package:gebeya_test/models/make.dart';
import 'package:gebeya_test/widgets/makeItemCard.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  List<Make>? makes=[];
  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMakes();
  }
  getMakes() async {
    List<Make>? makes1=[];

    var response= await  ApiService().getMakes();
    if (response==null){

    } else{
     // print(response[0]['name']);
      print(Make.fromJsonList( response ).first.imageUrl);
      setState(() {
        makes=Make.fromJsonList( response );
      });

  //    response.fo

    }

  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: Icon(Icons.widgets_sharp,color: Colors.black,),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title,style: TextStyle(color: Colors.black),),
        actions: [Icon(Icons.shopping_bag_outlined,color: Colors.black,)],
      ),
      body:

        SingleChildScrollView(
    child:
        Column(

          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[


          Container(
            alignment: Alignment.topCenter,

          height: 150,

          child:
          ListView.builder(

              itemCount: makes!.length,
            //  shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Expanded(
                  child: MakeItem(
                    name: makes![index].name,
                    imageUrl: makes![index].imageUrl,


                ));
              },
            )),



          ],
        ),
      ))

    ;
  }
}
