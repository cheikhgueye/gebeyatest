

class Make {
  final int id;
  final String name;
  final String imageUrl;


  const Make({
    required this.id,
    required this.name,
    required this.imageUrl,

  });

  static List<Make> fromJsonList(List list) {
    if (list == null) return [];
    return list.map((item) =>Make.fromJson(item)).toList();
  }
  factory Make.fromJson(Map<String, dynamic> json) {
    return Make(
      id: json['id'],
      name: json['name'],
      imageUrl: json['imageUrl'],

    );
  }


}