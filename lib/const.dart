import 'dart:ui';
import 'package:flutter/material.dart';
final themeColor = Color(0xfff5a623);
final primaryColor = Color(0xff203152);
final greyColor = Color(0xffaeaeae);
final greyColor2 = Color(0xffE8E8E8);
class APIConstants {
    // static const String API_BASE_URL = 'https://196.207.202.51:8081/';
    static const String API_BASE_URL = 'https://api-prod.autochek.africa/v1/';

}
