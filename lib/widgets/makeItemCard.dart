
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MakeItem extends StatelessWidget {



  final Function? onClick;
  final String? name;
  final String? imageUrl;

  const  MakeItem  ({
    Key? key,
    this.name, this.imageUrl,  this.onClick,

  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [


          //  Image.network(imageUrl!,width: 100,height: 100,),
            Container(
              width: 100,
              height: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: Colors.grey.shade50,
          image: DecorationImage(
            image: NetworkImage(imageUrl!),
            fit: BoxFit.fill
          ),

        ),
             child:   Container(
               width: 20,
               height: 20,
               child:  SvgPicture.network(

                 imageUrl!,
                 semanticsLabel: 'A shark?!',
                 width: 50,
                 height: 50,

               ),
             ),
            ),
            SizedBox(height: 10,),
          Center(
            child:  Container(
              //    width: 100,
              alignment: Alignment.center,


              child:  Text(name!,style: TextStyle(fontWeight: FontWeight.bold),),
            ),
          )

          ],
        ),
        // trailing: Icon(Icons.arrow_forward_outlined,color: Colors.black,),



    );

  }
}
